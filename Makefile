

CC=gcc
TARGET=read

all: os
	$(CC) -o $(TARGET) *.o

os: hexdump.o read_os.o
syscall: hexdump.o read_syscall.o

clean:
	rm *.o
