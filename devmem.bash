#!/bin/bash
devmem=devmem2
base=$1
size=$2
wsize=8


if [ "$2" = "" ]; then
	echo    "Usage: $0 <base> <size> [type]"
	echo -e "\ttype    : access operation type : [b]yte, [h]alfword, [w]ord"
	exit
fi

case $3 in
	b|byte)
		wsize=1;
;;
	h|halfword)
		wsize=2;
;;
	w|word)
		wsize=8;
esac

for ((i=0; i < $(( $size/$wsize )); i++))
do
	a=$(( $i*$wsize ))
# uncomment this for human readable stdout
#	if [ $(( $i%16 )) -eq 0 ]; then printf '\n%08x: ' $a; fi;
	value=$($devmem $(printf '0x%x' $(( $base+$a )) ) $3 | grep -oP ': (0x[\da-fA-F]+)' | sed 's/^.\+0x\([0-9a-fA-F]\+\).\+\?/ \1/g' | tr -d ' \n');
	printf '%04x ' "0x$value"
done

echo

