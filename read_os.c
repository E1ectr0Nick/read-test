//#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "hexdump.h"

int main()
{
	size_t size = 0x110;
	unsigned char buf[size];
	FILE *f;

	memset(buf, 0, size);
	f = fopen("/dev/rawmem", "rb");

	fread(buf, sizeof(*buf), size, f);
	hexdump(buf, size, 0);

	fseek(f, 0x400 /*offset*/, SEEK_SET); //whence: SEEK_SET, SEEK_END, or SEEK_CUR

	fread(buf, sizeof(*buf), size, f);
	hexdump(buf, size, 0);

	fclose(f);
}
