#!/usr/bin/fish

for i in (seq 0 0x100);
	if [ (math "$i%32 ") -eq 0 ] ; echo -ne "\n" (math -b16 $i) ': '; end;
	sudo devmem2 (math -b16 "0x1000000+$i") b | grep -oP ': (0x[\da-f]+)' | sed 's/^.\+0x\([0-9a-f]\+\).\+\?/ \1/g' | tr -d '\n';
end;
