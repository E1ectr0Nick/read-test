#include <stdio.h>

void hexdump(char *buf, size_t len, size_t offset)
{
	unsigned char line[17];
	char c;
	if(len < 16) return;
	line[16] = 0;
	for(size_t i = offset; i <= len-16; i += 16)
	{
		for(int j = 0; j < 16; j++)
		{
			c = buf[i+j];
			line[j] = (c >= ' ' && c <= '~' && c != '|') ? c : '.';
		}

//		while(line[15-(i%16)] = (c >= ' ' && c <= '~' && c != '|') ? c : '.', 15-(i%16))
//			c = buf[++i];
//		i-=15;
		printf("%08x: %02x %02x %02x %02x %02x %02x %02x %02x  ", i,
			buf[i], buf[i+1], buf[i+2], buf[i+3], buf[i+4], buf[i+5], buf[i+6], buf[i+7]);
		printf("%02x %02x %02x %02x %02x %02x %02x %02x  |%s|\n",
			buf[i+8], buf[i+9], buf[i+10], buf[i+11], buf[i+12], buf[i+13], buf[i+14], buf[i+15], line);
	}

}


